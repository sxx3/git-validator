# smells

## 0.0.7

### Patch Changes

- 2e702e2: chore: upgrade deps

## 0.0.6

### Patch Changes

- d832aad: chore: upgrade deps

## 0.0.5

### Patch Changes

- 36d1fb0: chore: upgrade deps
- 905e445: chore: update deps

## 0.0.4

### Patch Changes

- 55cf874: chore: upgrade typescript-eslint to v8

## 0.0.3

### Patch Changes

- 7c26453: fix: correct the peerDependencies

## 0.0.2

### Patch Changes

- 9494e2d: fix(smells): add hash bang to cli entrance

## 0.0.1

### Patch Changes

- 28c8284: feat: migrate analyze command from `git-validator` to `smells`
