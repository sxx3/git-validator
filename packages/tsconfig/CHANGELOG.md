# @fenge/tsconfig

## 0.3.1

### Patch Changes

- 2e702e2: chore: upgrade deps
- 58e95d9: chore: upgrade deps

## 0.3.0

### Minor Changes

- cb2a822: feat: allow importing ts files

## 0.2.1

### Patch Changes

- 2560801: chore: upgrade deps

## 0.2.0

### Minor Changes

- 16bf9a1: refactor(tsconfig): remove `./cli` entrance in package.json

## 0.1.1

### Patch Changes

- b1fc095: docs(tsconfig): update `Best Practices` guide to support TS 5.7+

## 0.1.0

### Minor Changes

- 514733a: feat(tsconfig): migrate from `@git-validator/tsconfig` to `@fenge/tsconfig`
