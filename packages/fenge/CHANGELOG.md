# fenge

## 0.5.1

### Patch Changes

- Updated dependencies [17936c7]
  - @fenge/eslint-config@0.5.8

## 0.5.0

### Minor Changes

- 43f9dfa: chore: upgrade eslint to v9

### Patch Changes

- 2e702e2: chore: upgrade deps
- Updated dependencies [2e702e2]
- Updated dependencies [58e95d9]
- Updated dependencies [10b7f69]
- Updated dependencies [10748a1]
  - @fenge/eslint-config@0.5.7
  - @fenge/tsconfig@0.3.1
  - @fenge/prettier-config@0.2.0

## 0.4.2

### Patch Changes

- 0ea3682: chore: upgrade deps
- Updated dependencies [1c35021]
- Updated dependencies [0ea3682]
  - @fenge/eslint-config@0.5.6
  - @fenge/prettier-config@0.1.4

## 0.4.1

### Patch Changes

- @fenge/eslint-config@0.5.5

## 0.4.0

### Minor Changes

- cb2a822: feat(fenge): improve compatible with TS 5.7

  TypeScript 5.7 add a feature that we can import from relative ts file. That means, from now on, we can write completely ts. TypeScript now is a 100% programming language, rather than a enhancement of JavaScript.

### Patch Changes

- Updated dependencies [cb2a822]
- Updated dependencies [cb2a822]
- Updated dependencies [cb2a822]
  - @fenge/eslint-config@0.5.4
  - @fenge/tsconfig@0.3.0

## 0.3.3

### Patch Changes

- 2560801: chore: upgrade deps
- Updated dependencies [d0e9357]
- Updated dependencies [d0e9357]
- Updated dependencies [2560801]
- Updated dependencies [d0e9357]
  - @fenge/eslint-config@0.5.3
  - @fenge/tsconfig@0.2.1

## 0.3.2

### Patch Changes

- Updated dependencies [9fcef9d]
- Updated dependencies [3f45e5f]
  - @fenge/eslint-config@0.5.2

## 0.3.1

### Patch Changes

- Updated dependencies [57d1dfe]
- Updated dependencies [553928a]
- Updated dependencies [8c4b1d4]
- Updated dependencies [6c0185f]
- Updated dependencies [9bb1dd5]
  - @fenge/eslint-config@0.5.1

## 0.3.0

### Minor Changes

- a3b985c: refactor: rename `Builder` methods to be camel case
- 8b468ec: refactor: remove `append` property for Builder. user can use append method

### Patch Changes

- Updated dependencies [d832aad]
- Updated dependencies [6c64129]
- Updated dependencies [692d3e8]
- Updated dependencies [561ecc5]
- Updated dependencies [9cde584]
- Updated dependencies [a3b985c]
- Updated dependencies [b26463e]
- Updated dependencies [16bf9a1]
- Updated dependencies [8b468ec]
- Updated dependencies [778a198]
- Updated dependencies [9b1f20a]
- Updated dependencies [4056ab2]
- Updated dependencies [e36c13c]
  - @fenge/eslint-config@0.5.0
  - prettier-ignore@0.2.0
  - @fenge/tsconfig@0.2.0

## 0.2.1

### Patch Changes

- Updated dependencies [1957781]
  - @fenge/eslint-config@0.4.1

## 0.2.0

### Minor Changes

- c7a01e8: refactor(eslint-config): replace `override` and `extend` properties with `append` property for overriding or extending the built-in rules
- db82f69: feat(fenge): use local ESLint and Prettier first
- be7e3d5: chore(fenge): upgrade `prettier` to 3.4.1
- 4c6fa59: refactor(fenge): optimize tsconfig exports and rename tsconfig.json to index.json

### Patch Changes

- 905e445: chore: update deps
- Updated dependencies [5adeeed]
- Updated dependencies [c7a01e8]
- Updated dependencies [36d1fb0]
- Updated dependencies [3605e14]
- Updated dependencies [c5a6425]
- Updated dependencies [d54f6af]
- Updated dependencies [84944a3]
- Updated dependencies [0d71378]
- Updated dependencies [740fdb7]
- Updated dependencies [a5fb6f8]
- Updated dependencies [41c783a]
- Updated dependencies [b1fc095]
- Updated dependencies [3f4e737]
- Updated dependencies [1f5fce3]
- Updated dependencies [905e445]
- Updated dependencies [51502cc]
- Updated dependencies [af6e004]
- Updated dependencies [2f12ec2]
- Updated dependencies [cd5aead]
  - @fenge/prettier-config@0.1.3
  - @fenge/eslint-config@0.4.0
  - @fenge/types@0.2.0
  - @fenge/tsconfig@0.1.1
  - prettier-ignore@0.1.4

## 0.1.8

### Patch Changes

- Updated dependencies [1848169]
- Updated dependencies [9f04b44]
- Updated dependencies [55cf874]
  - @fenge/prettier-config@0.1.2
  - @fenge/eslint-config@0.3.0

## 0.1.7

### Patch Changes

- d58f9dd: feat(fenge): add `--config` option to sub-command `lint` and `format`
- 3fd7084: feat(fenge): support `--default` option to forcibly use built-in default config

## 0.1.6

### Patch Changes

- Updated dependencies [974ac53]
  - @fenge/eslint-config@0.2.1

## 0.1.5

### Patch Changes

- 1c3fa77: chore: update deps
- Updated dependencies [1c3fa77]
- Updated dependencies [a2d5f7c]
  - @fenge/eslint-config@0.2.0

## 0.1.4

### Patch Changes

- 3783bd5: feat(fenge): `format` and `lint` properties in `fenge.config.js` are additionally accepting a function now

## 0.1.3

### Patch Changes

- a9afc71: feat(fenge): support `--config` option
- Updated dependencies [9e94865]
  - @fenge/prettier-config@0.1.1
  - @fenge/eslint-config@0.1.3

## 0.1.2

### Patch Changes

- 21a6435: fix(fenge): throw error when installing git hook files if it is already existing
- c8cf96d: chore: upgrade deps
- ee3b1e3: feat(fenge): add uninstall command
- Updated dependencies [e7376a3]
- Updated dependencies [c8cf96d]
- Updated dependencies [84e0219]
  - @fenge/eslint-config@0.1.2

## 0.1.1

### Patch Changes

- @fenge/eslint-config@0.1.1

## 0.1.0

### Minor Changes

- 27bbb4a: feat: migrate `git-validator` to fenge

  IMPORTANT: We migrate all the features from `git-validator` to `fenge` now. `git-validator` will be deprecated later.

### Patch Changes

- Updated dependencies [514733a]
- Updated dependencies [67f298b]
- Updated dependencies [db5931d]
- Updated dependencies [b5b70e3]
  - @fenge/tsconfig@0.1.0
  - @fenge/types@0.1.0
  - @fenge/prettier-config@0.1.0
  - @fenge/eslint-config@0.1.0
