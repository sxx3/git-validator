import "./array-is-array.d.ts";
import "./json-parse.d.ts";
import "./new-map.d.ts";
import "./new-promise-reject.d.ts";
import "./promise-catch.d.ts";
import "./promise-then.d.ts";
