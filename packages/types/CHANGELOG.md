# @fenge/types

## 0.2.0

### Minor Changes

- 3605e14: refactor(types): rename `map-constructor` to `new-map`
- d54f6af: refactor(types): separate `promise-catch.d.ts` into `promise-catch.d.ts` and `promise-then.d.ts`
- a5fb6f8: feat(types): disallow to reject a non Error variable when call `new Promise()`

## 0.1.0

### Minor Changes

- 67f298b: feat(types): migrate from `@git-validator/types` to `@fenge/types`
