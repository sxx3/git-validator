# eslint-plugin-publint

## 0.1.1

### Patch Changes

- 0c3b8f0: chore: add meta to rules

## 0.1.0

### Minor Changes

- 60c56ed: feat: ignore `package.json`s whose `private` is true

## 0.0.5

### Patch Changes

- cd5aead: chore: no significant changes

## 0.0.4

### Patch Changes

- c8cf96d: chore: upgrade deps

## 0.0.3

### Patch Changes

- 44584fa: refactor(eslint-plugin-publint,eslint-plugin-pkg-json): change export default to statement

## 0.0.2

### Patch Changes

- a94ac0a: chore: upgrade deps

## 0.0.1

### Patch Changes

- 36a745f: feat(eslint-plugin-publint): finish
