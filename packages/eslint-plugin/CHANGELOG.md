# @fenge/eslint-plugin

## 0.2.0

### Minor Changes

- d2e61d3: feat: `no-top-level-arrow-function` only report when the function body is `BlockStatement`

## 0.1.3

### Patch Changes

- d0e9357: feat: add rule `no-nested-function`
- d0e9357: feat: add rule `no-top-level-arrow-function`
- d0e9357: feat: add rule `no-nested-class`

## 0.1.2

### Patch Changes

- 51502cc: feat: add rule `@fenge/no-jsx-in-non-jsx-file`
- 713d9a2: feat(eslint-plugin): support glob for `call-arguments-length` option

## 0.1.1

### Patch Changes

- e3bed52: fix(eslint-plugin): report on the variables that are not declared

## 0.1.0

### Minor Changes

- 0beda11: feat(eslint-plugin): migrate from `@git-validator/eslint-plugin` to `@fenge/eslint-plugin`
