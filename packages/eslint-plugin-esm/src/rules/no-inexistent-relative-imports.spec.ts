import { fileURLToPath } from "node:url";
import { test } from "@fenge/dev-utils";
import { noInexistentRelativeImports } from "./no-inexistent-relative-imports.ts";

const filename = fileURLToPath(import.meta.url);

const valid = [
  "import foo from 'foo'",

  // with ext
  "import foo from './no-inexistent-relative-imports.spec.ts'",
  "import foo from '../rules'",
  "import foo from '.'",
  "import foo from './'",
  "import foo from '..'",
  "import foo from '../'",
].map((code) => ({ code, filename }));

const invalid = [
  "import foo from './no-inexistent-relative-imports.spec.js'",
  // without ext
  "import foo from './no-inexistent-relative-imports.spec'",
  "import foo from './inexistent-file'",
  "import foo from '../inexistent-file'",
].map((code) => ({ code, filename }));

test({ valid, invalid, ...noInexistentRelativeImports });
