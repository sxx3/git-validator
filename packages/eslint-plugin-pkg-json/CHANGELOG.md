# eslint-plugin-pkg-json

## 0.1.3

### Patch Changes

- a2e9f23: fix(eslint-plugin-pkg-json): disallow `prepublish` script

## 0.1.2

### Patch Changes

- cd5aead: chore: no significant changes

## 0.1.1

### Patch Changes

- 44584fa: refactor(eslint-plugin-publint,eslint-plugin-pkg-json): change export default to statement

## 0.1.0

### Minor Changes

- d564959: refactor: migrate from `@git-validator/eslint-plugin-packagejson` to `eslint-plugin-pkg-json`
