<!-- prettier-ignore-start -->
# no-const-enum

undefined

## Rule Details

### Fail

```ts
const enum E {}
```

### Pass

```ts
enum E {}
```
<!-- prettier-ignore-end -->
