# @fenge/eslint-plugin-ts

## 0.2.2

### Patch Changes

- 2e702e2: chore: upgrade deps

## 0.2.1

### Patch Changes

- 1c35021: feat: add rule `no-misuse-spreading-parameter`

## 0.2.0

### Minor Changes

- 58c81c0: feat(eslint-plugin-ts): remove `no-declares-in-ts-file` and create new rule `no-declares`

## 0.1.0

### Minor Changes

- 615a726: feat(eslint-plugin-ts): migrate from `@git-validator/eslint-plugin-ts` to `@fenge/eslint-plugin-ts`
