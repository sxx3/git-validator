# @fenge/prettier-config

## 0.2.0

### Minor Changes

- 10748a1: refactor: move all `plugins` to `overrides`

## 0.1.4

### Patch Changes

- 0ea3682: chore: upgrade deps

## 0.1.3

### Patch Changes

- 5adeeed: fix: add missing peerDependencies and peerDependenciesMeta
- 36d1fb0: chore: upgrade deps

## 0.1.2

### Patch Changes

- 1848169: chore: upgrade deps

## 0.1.1

### Patch Changes

- 9e94865: chore: upgrade deps

## 0.1.0

### Minor Changes

- db5931d: feat(prettier-config): migrate from `@git-validator/prettier-config` to `@fenge/prettier-config`
