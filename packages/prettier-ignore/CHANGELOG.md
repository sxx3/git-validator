# prettier-ignore

## 0.2.0

### Minor Changes

- 561ecc5: chore(prettier-ignore): remove `.changeset`

## 0.1.4

### Patch Changes

- cd5aead: chore: no significant changes

## 0.1.3

### Patch Changes

- 07366db: chore: upgrade deps

## 0.1.2

### Patch Changes

- ab8226c: chore: upgrade deps

## 0.1.1

### Patch Changes

- d0dce38: chore: bump version

## 0.1.0

### Minor Changes

- 207cf6a: feat(prettier-ignore): init
